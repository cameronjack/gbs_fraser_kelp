#!/usr/bin/env python

import argparse

"""
    Name: filter_samples.py
    Author: Cameron Jack, 2017, ANU Bioinformatics Consultancy
    Contact: cameron.jack@anu.edu.au
    License: None. Do with it as you will
"""

def main():
    """ 
        Stacks Populations seems to trim sample names to fixed lengths.
        This script fixes these names in the .phy file
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--phy', required=True, help='Path to .phy file to fix')
    parser.add_argument('--map', required=True, help='Path to sample map file')
    parser.add_argument('--fixed', required=True, help='Path to output fixed '+\
            '.phy file')
    args = parser.parse_args()

    sample_names = []
    with open(args.map, 'r') as f:
        for line in f:
            cols = line.strip().split('\t')
            # PHYLIP format allows exactly 10 chars for sample names
            template = [' ' for i in range(10)]
            sample_name = cols[0].split('_')[0][4:]
            for i, symbol in enumerate(sample_name): 
                template[i] = symbol
            sample_names.append(''.join(template))

    with open(args.fixed, 'w') as out:
        with open(args.phy, 'r') as f:
            for i, line in enumerate(f):
                if i == 0:
                    out.write(line)
                    continue
                cols = line.strip().split()
                cols[0] = sample_names[i-1]
                # sequences are butted hard up against the sample names
                out_line = ''.join(cols) + '\n'
                out.write(out_line)


if __name__ == '__main__':
    main()
