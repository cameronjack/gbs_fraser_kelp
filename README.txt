Files for Antarctic Kelp study

filter_samples.py - Iteratively remove uninformative sites and then throw out samples with less than N% valid site information (default 5%)
fix_sample_names.py - stacks will truncate long names when writing PHYLIP format, this fixes them
make_whitelist.py - collects the locus IDs that are considered valid following population level filtering
pipe_NSO.sh - the main pipeline following demultiplexing, running Stacks and iqtree
kelp_NSO_axe_barcodes.csv - the barcodes for each sample in the required format for AXE to demultiplex
kelp_NSO_pop_map.csv - Samples grouped by geographic location and collection
kelp_NSO_indiv_map.csv - Samples as individual populations for Stacks to translate to PHYLIP format for IQtree
gdugbs/config.yml - config file for the AXE demultiplex & Trimit cleanup
AXE barcodes.xls - Excel format sample barcodes used for demultiplexing
