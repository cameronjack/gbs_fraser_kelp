#!/usr/bin/env python

import argparse

"""
    Name: filter_samples.py
    Author: Cameron Jack, 2017, ANU Bioinformatics Consultancy
    Contact: cameron.jack@anu.edu.au

    Reads the a Stacks Populations sumstats.csv file for a batch 
    and outputs a SNP whitelist for use with an individual based 
    Populations list

    There is no license for this code. Do with this as you will.
"""

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--sumstats', required=True, help='Path to sumstats.tsv file')
    parser.add_argument('--whitelist', required=True, help='Path to output whitelist file')
    args = parser.parse_args()
    
    loci_columns = set()
    with open(args.sumstats, 'r') as f:
        for line in f:
            if line.startswith('#'):
                continue  # header line
            cols = line.strip().split('\t')
            locus = cols[1]
            column = cols[4]
            loci_columns.add(tuple([locus, column]))

    with open(args.whitelist, 'w') as out:
        for (l, c) in loci_columns:
            out.write(l + '\t' + c + '\n')

 
if __name__ == '__main__':
    main()
