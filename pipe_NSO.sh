#!/bin/bash

######
# Process GBS data de novo with Stacks.
# 1. Run the section from USTACKS to RXSTACKS_DIST
# 2. Check log likelihoods
# 3. Run the RXSTACKS through POPULATIONS steps.
# 4. Iterate through POPULATIONS and INDIVIDUALS with settings that keep most
# data points while clearly removing minimally informative or unreliable SNPs
# 5. The IQ-tree step should be run last on its own and includes a step to 
# remove uninformed samples (default <5% of informative SNP sites) 
#
# This script is built for a cluster running SGE. Simply strip out the qsub 
# content to run locally. Total processing time on a modern machine should 
# be in the order of 18 hours.
#
# All scripts and analysis performed by Cameron Jack, 2017.
# ANU Bioinformatics Consultancy, cameron.jack@anu.edu.au
######

if [ ! -d ustacks_NSO ] ; then
    mkdir -p ustacks_NSO
fi
if [ ! -d refined_NSO ] ; then
    mkdir -p refined_NSO
fi

# QCD = demultiplexed quality controlled data directory 
$QCD='qcd'

USTACKS=0
if [ $USTACKS == 1 ] ; then
    count=1
    for f in $QCD/*.fastq.gz
    do 
        qsub -pe threads 12 -cwd -N 'ustacks' -b y \
            ustacks -f $f -o ustacks_NSO -i $count -m 5 -M 3 -d -p 12
        count=$((count + 1))
    done
fi

CSTACKS=0
if [ $CSTACKS == 1 ] ; then
    qsub -pe threads 12 -cwd -hold_jid 'ustacks' -N 'cstacks' -m e -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g cstacks -P ustacks_NSO -p 12 -n 4 -M kelp_NSO_pop_map.csv 
fi

SSTACKS=0
if [ $SSTACKS == 1 ] ; then
    qsub -pe threads 12 -cwd -hold_jid 'cstacks' -N 'sstacks' -m e -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g sstacks -P ustacks_NSO -p 12 -M kelp_NSO_pop_map.csv
fi

RXSTACKS_DIST=0
if [ $RXSTACKS_DIST == 1 ] ; then
    qsub -pe threads 12 -cwd -hold_jid 'sstacks' -N 'rxstacks' -m e -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g rxstacks -P ustacks_NSO -o refined_NSO -t 12 --lnl_dist \
        --conf_filter --conf_lim=0.25 --prune_haplo --lnl_lim -20 --max_haplo 4
fi

### STOP HERE and run LOG_LIKELIHOODS separately, review and rerun ###

LOG_LIKELIHOODS=0
if [ $LOG_LIKELIHOODS == 1 ] ; then
    cat refined_NSO/batch_1.rxstacks_lnls.tsv |grep -v "^#" | \
        awk '{bucket=(int($2)); lnls[bucket] += 1} END { for (bucket in lnls) print bucket, "\t", lnls[bucket]}' \
        | sort -n > lnls.tsv
fi

### Carry on with finalised RXSTACKS parameters ###

RXSTACKS=0
if [ $RXSTACKS == 1 ] ; then
    qsub -pe threads 12 -cwd -hold_jid 'sstacks' -N 'rxstacks' -m e -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g rxstacks -P ustacks_NSO -o refined_NSO -t 12 \
        --conf_filter --conf_lim=0.25 --prune_haplo --lnl_lim -20 --max_haplo 4
fi

CSTACKS2=0
if [ $CSTACKS2 == 1 ] ; then
    qsub -pe threads 12 -cwd -hold_jid 'rxstacks' -N 'cstacks2' -m e -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g cstacks -P refined_NSO -p 12 -n 4 -M kelp_NSO_pop_map.csv
fi

SSTACKS2=0
if [ $SSTACKS2 == 1 ] ; then
    qsub -pe threads 12 -cwd -hold_jid 'cstacks2' -N 'sstacks2' -m e -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g sstacks -P refined_NSO -p 12 -M kelp_NSO_pop_map.csv
fi

POPULATIONS=1
if [ $POPULATIONS == 1 ] ; then
    # Population level... there are 24 pop groups
    # run this first to filter loci, then remap the samples to separate population groups and run again
    qsub -pe threads 12 -cwd -hold_jid 'sstacks2' -N 'populations' -m e -M 'cameron.jack@anu.edu.au' -b y\
        -l h_vmem=3.2g,virtual_free=3.1g populations -P refined_NSO -t 12 -M kelp_NSO_pop_map.csv \
        -p 2 -r 0.2 --min_maf 0.2 -m 10 --phylip_var 
    qsub -cwd -N 'whitelist' -hold_jid 'populations' -b y \
        python make_whitelist.py --sumstats refined_NSO/batch_1.sumstats.tsv \
        --whitelist kelp_NSO_p2_20perc_maf02_m10.whitelist
fi

INDIVIDUALS=1
if [ $INDIVIDUALS == 1 ] ; then
    qsub -pe threads 12 -cwd -N 'individuals' -m e \
        -M 'cameron.jack@anu.edu.au' -l h_vmem=3g,virtual_free=2.9g \
        -hold_jid 'whitelist' -b y\
        populations -P refined_NSO -t 12 -M kelp_NSO_indiv_map.csv \
        --phylip_var -W kelp_NSO_p2_20perc_maf02_m10.whitelist
fi

### Run IQTREE seperately, after everything else has finished
IQTREE=1
if [ $IQTREE == 1 ] ; then
    cat refined_NSO/batch_1.phylip | grep -v '^#' > ant_NSO_p2_20perc_maf02_m10.phy
    python fix_sample_names.py --phy ant_NSO_p2_20perc_maf02_m10.phy --map kelp_NSO_indiv_map.csv \
        --fixed ant_NSO_p2_20perc_maf02_m10_fixed.phy

    # Antartic kelp paper 2017
    qsub -cwd -V -N 'filter' -hold_jid 'pre_iqtree' -b y \
        "python filter_samples.py \
        --phy ant_NSO_p2_20perc_maf02_m10_fixed.phy \
        --keep ant_NSO_filt05_p2_20perc_maf02_m10.phy --cutoff 0.05"
    qsub -pe threads 12 -cwd -N 'iqtree' -m e -M 'cameron.jack@anu.edu.au' -b y\
       	-l h_vmem=3g,virtual_free=2.9g -hold_jid 'filter' iqtree-omp -nt 12 \
        -s ant_NSO_filt05_p2_20perc_maf02_m10.phy -st DNA -m MFP -bb 1000 \
        -pre ant_NSO_filt05_p2_20perc_maf02_m10.phy

fi
